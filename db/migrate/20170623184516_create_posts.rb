class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.references :user, index: true
      t.string :link
      t.text :body

      t.timestamps
    end
  end
end
