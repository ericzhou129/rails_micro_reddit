class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.references :post, index: true
      t.references :user, index: true
      t.text :body

      t.timestamps
    end
  end
end
